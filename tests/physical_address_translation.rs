#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(blog_os::test_runner)]
#![reexport_test_harness_main = "test_main"]

use bootloader::{entry_point, BootInfo};
use core::panic::PanicInfo;
use spin::Once;
use x86_64::{
    structures::paging::{OffsetPageTable, Translate},
    PhysAddr, VirtAddr,
};

static MAPPER: Once<OffsetPageTable> = Once::new();

static BUFFER: [u8; 64] = [0u8; 64];

entry_point!(test_kernel_main);
fn test_kernel_main(boot_info: &'static BootInfo) -> ! {
    MAPPER.call_once(|| unsafe {
        blog_os::memory::init(VirtAddr::new(boot_info.physical_memory_offset))
    });
    test_main();

    blog_os::hlt_loop()
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    blog_os::test_panic_handler(info)
}

#[test_case]
fn test_invalid() {
    assert_eq!(
        MAPPER.get().unwrap().translate_addr(VirtAddr::new(0x10)),
        None
    );
}

#[test_case]
fn test_valid_static() {
    assert!(matches!(
        MAPPER
            .get()
            .unwrap()
            .translate_addr(VirtAddr::from_ptr(BUFFER.as_ptr())),
        Some(_)
    ));
}

pub extern "C" fn bogus() {}

#[test_case]
fn test_valid_code() {
    assert!(matches!(
        MAPPER
            .get()
            .unwrap()
            .translate_addr(VirtAddr::from_ptr(&bogus)),
        Some(_)
    ));
}

#[test_case]
fn test_valid_stack() {
    let tmp = 0;
    assert!(matches!(
        MAPPER
            .get()
            .unwrap()
            .translate_addr(VirtAddr::from_ptr(&tmp)),
        Some(_)
    ));
}

#[test_case]
fn test_ident_map_vga() {
    assert_eq!(
        MAPPER
            .get()
            .unwrap()
            .translate_addr(VirtAddr::new(blog_os::vga_buffer::VGA_ADDR)),
        Some(PhysAddr::new(blog_os::vga_buffer::VGA_ADDR))
    );
}
