#![no_std]
#![no_main]

use blog_os::{exit_qemu, serial_print, serial_println, QemuExitCode};
use core::panic::PanicInfo;

#[no_mangle]
pub extern "C" fn _start() -> ! {
    should_fail();
    serial_println!("[test did not panic]\n");
    exit_qemu(QemuExitCode::Failed);
    blog_os::hlt_loop()
}

fn should_fail() {
    serial_print!("\nshould_panic::should_fail... ");
    assert_eq!(0, 1);
}

#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    serial_println!("[OK]\n");
    exit_qemu(QemuExitCode::Success);
    blog_os::hlt_loop()
}
