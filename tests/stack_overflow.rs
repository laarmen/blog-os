#![no_std]
#![no_main]
#![feature(abi_x86_interrupt)]
use blog_os::{exit_qemu, serial_print, serial_println, QemuExitCode};
use core::panic::PanicInfo;
use lazy_static::lazy_static;
use x86_64::structures::idt::InterruptDescriptorTable;

#[no_mangle]
pub extern "C" fn _start() -> ! {
    serial_print!("\nstack_overflow::stack_overflow... ");

    blog_os::gdt::init();
    init_test_idt();

    stack_overflow();
    exit_qemu(QemuExitCode::Failed);

    blog_os::hlt_loop()
}

fn stack_overflow() {
    static NO_TAILCALL: usize = 0;
    #[allow(unconditional_recursion)]
    fn infinite_recursive() {
        infinite_recursive();
        volatile::Volatile::new(&NO_TAILCALL).read();
    }
    infinite_recursive();
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    blog_os::test_panic_handler(info);
}

lazy_static! {
    static ref IDT: InterruptDescriptorTable = {
        let mut idt = InterruptDescriptorTable::new();
        unsafe {
            idt.double_fault
                .set_handler_fn(double_fault_handler)
                .set_stack_index(blog_os::gdt::DOUBLE_FAULT_IST_INDEX);
        }
        idt
    };
}

fn init_test_idt() {
    IDT.load()
}

extern "x86-interrupt" fn double_fault_handler(
    _stack_frame: x86_64::structures::idt::InterruptStackFrame,
    _error_code: u64,
) -> ! {
    serial_println!("[OK]\n");
    exit_qemu(QemuExitCode::Success);
    blog_os::hlt_loop()
}
