use x86_64::registers::control::Cr3;
use x86_64::structures::paging::{OffsetPageTable, PageTable};
use x86_64::VirtAddr;

/// Initialize a new OffsetPageTable
///
/// # Safety
///
/// This function is unsafe because the caller must guarantee that the
/// complete physical memory is mapped to virtual memory at the passed
/// `physical_memory_offset`. Also, this function must be only called once
/// to avoid aliasing `&mut` references (which is undefined behavior).
pub unsafe fn init(physical_memory_offset: VirtAddr) -> OffsetPageTable<'static> {
    unsafe {
        let lv4_table = active_level4_table(physical_memory_offset);
        OffsetPageTable::new(lv4_table, physical_memory_offset)
    }
}

unsafe fn active_level4_table(physical_memory_offset: VirtAddr) -> &'static mut PageTable {
    let (frame, _flags) = Cr3::read();
    let virt_frame_addr = physical_memory_offset + frame.start_address().as_u64();
    let table_ptr: *mut PageTable = virt_frame_addr.as_mut_ptr();
    unsafe { &mut *table_ptr }
}
