use core::ptr::NonNull;
use spin::Mutex;
use spin::MutexGuard;
use volatile::Volatile;

#[allow(dead_code)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum Color {
    Black = 0x0,
    Blue = 0x1,
    Green = 0x2,
    Cyan = 0x3,
    Red = 0x4,
    Magenta = 0x5,
    Brown = 0x6,
    LightGray = 0x7,
    DarkGray = 0x8,
    LightBlue = 0x9,
    LightGreen = 0xa,
    LightCyan = 0xb,
    LightRed = 0xc,
    Pink = 0xd,
    Yellow = 0xe,
    White = 0xf,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct ColorCode(u8);

impl ColorCode {
    pub const fn new(foreground: Color, background: Color) -> ColorCode {
        ColorCode((background as u8).wrapping_shl(4) + foreground as u8)
    }
}

#[test_case]
fn test_colorcode_hardcoded_cyanonblack() {
    // In the tutorial we started with a hardcoded 0x0b color,
    // and the text was light cyan on a black background.

    assert_eq!(ColorCode::new(Color::LightCyan, Color::Black).0, 0x0b);
}

#[repr(packed)]
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct VgaChar {
    pub character: u8,
    pub color: ColorCode,
}

impl VgaChar {
    pub fn new(character: u8, color: ColorCode) -> VgaChar {
        VgaChar { character, color }
    }
}

#[test_case]
fn test_cell_layout() {
    assert_eq!(core::mem::size_of::<VgaChar>(), core::mem::size_of::<u16>());
}

const BUFFER_HEIGHT: usize = 25;
const BUFFER_WIDTH: usize = 80;

pub struct Writer<'a> {
    state: MutexGuard<'a, WriterState>,
}

impl<'a> Writer<'a> {
    fn new(state: &Mutex<WriterState>) -> Writer<'_> {
        Writer {
            state: state.lock(),
        }
    }
}

type VgaScreen = [[VgaChar; BUFFER_WIDTH]; BUFFER_HEIGHT];

impl<'a> Writer<'a> {
    fn buffer(&mut self) -> Volatile<&mut VgaScreen> {
        unsafe {
            let ptr = NonNull::new_unchecked(self.state.buffer as *mut VgaChar);
            Volatile::new(ptr.cast::<VgaScreen>().as_mut())
        }
    }

    pub fn clear_row(&mut self, row: usize) {
        let color_code = self.state.color_code;
        assert!(row < BUFFER_HEIGHT);
        self.buffer()
            .as_mut_slice()
            .index_mut(row)
            .as_mut_slice()
            .copy_from_slice(&[VgaChar::new(b' ', color_code); BUFFER_WIDTH])
    }

    pub fn new_line(&mut self) {
        for row in 1..BUFFER_HEIGHT {
            self.buffer()
                .as_mut_slice()
                .copy_within(row..row + 1, row - 1)
        }
        self.clear_row(BUFFER_HEIGHT - 1);

        self.state.column_position = 0;
    }

    pub fn write_byte(&mut self, byte: u8) {
        match byte {
            b'\n' => self.new_line(),
            byte => {
                if self.state.column_position >= BUFFER_WIDTH {
                    self.new_line()
                }

                let row = BUFFER_HEIGHT - 1;
                let col = self.state.column_position;

                let color_code = self.state.color_code;

                self.buffer()
                    .as_mut_slice()
                    .index_mut(row)
                    .as_mut_slice()
                    .index_mut(col)
                    .write(VgaChar::new(byte, color_code));
                self.state.column_position += 1;
            }
        }
    }

    pub fn write_string(&mut self, string: impl AsRef<str>) {
        let string = string.as_ref();
        string
            .chars()
            .map(|c| {
                if c.is_ascii() {
                    u32::from(c) as u8
                } else {
                    0xfe
                }
            })
            .for_each(|b| self.write_byte(b))
    }
}

struct WriterState {
    column_position: usize,
    color_code: ColorCode,
    buffer: u64,
}

pub const VGA_ADDR: u64 = 0xb8000;

static WRITER: Mutex<WriterState> = Mutex::new(WriterState {
    column_position: 0,
    color_code: ColorCode::new(Color::White, Color::Black),
    buffer: VGA_ADDR,
});

pub fn with_writer(f: impl FnOnce(&mut Writer)) {
    f(&mut Writer::new(&WRITER))
}

impl<'a> core::fmt::Write for Writer<'a> {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        self.write_string(s);
        Ok(())
    }
}

#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => ($crate::vga_buffer::_print(format_args!($($arg)*)));
}

#[macro_export]
macro_rules! println {
    () => ($crate::print!("\n"));
    ($($arg:tt)*) => ($crate::print!("{}\n", format_args!($($arg)*)));
}

#[doc(hidden)]
pub fn _print(args: core::fmt::Arguments) {
    use core::fmt::Write;
    x86_64::instructions::interrupts::without_interrupts(|| {
        with_writer(|w| w.write_fmt(args).unwrap())
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    trait GetChar {
        fn get_char(&self, row: usize, column: usize) -> u8;
        fn get_full_char(&self, row: usize, column: usize) -> VgaChar;
    }

    impl GetChar for Volatile<&mut VgaScreen> {
        fn get_char(&self, row: usize, column: usize) -> u8 {
            self.get_full_char(row, column).character
        }

        fn get_full_char(&self, row: usize, column: usize) -> VgaChar {
            self.as_slice().index(row).as_slice().index(column).read()
        }
    }

    #[test_case]
    fn test_println_output() {
        use core::fmt::Write;
        let s = "Some test string that fits on a single line";
        x86_64::instructions::interrupts::without_interrupts(|| {
            with_writer(|w| {
                writeln!(w, "\n{}", s).expect("Write failed");

                for (i, c) in s.chars().enumerate() {
                    let screen_char = w.buffer().get_char(BUFFER_HEIGHT - 2, i);
                    assert_eq!(char::from(screen_char), c);
                }
            })
        })
    }

    #[test_case]
    fn test_println_many() {
        // Just a panic test
        for _ in 0..200 {
            println!("test_println_many output");
        }
    }
}
